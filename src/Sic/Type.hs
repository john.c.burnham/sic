module Sic.Type where


import           Data.Char

type Symb = Int

data Term =
    Abs Symb Term
  | App Term Term
  | Par Term Term
  | Let Symb Symb Term Term
  | Var Symb
  deriving Eq

-- substitution cell
data Sub = (:/) Symb Term

instance Show Term where
  show (Var n) = showVar n
  show (Abs h b) = concat ["\\", showVar h, ".", show b]
  show (App l r) = concat ["(", show l, " ", show r, ")"]
  show (Par l r) = concat ["(", show l, ",", show r, ")"]
  show (Let p q t b) = concat
    ["let (", showVar p, ",", showVar q, ") = ", show t, " in ", show b]

showVar :: Int -> String
showVar n = go (n + 1)
  where
    go 0 = []
    go n = chr (97 + (n `mod` 26) - 1) : go (n `div` 26)
