{-# LANGUAGE LambdaCase        #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Sic.Eval where

import           Control.Monad.Trans.State.Lazy
import           Data.Text                      (Text, snoc)
import           Sic.Type

-- substitute
(#) :: Term -> Sub -> Term
(#) f (x :/ a) = case f of
  Abs y g       -> Abs y $ g # x :/ a
  App m n       -> App (m # x :/ a) (n # x :/ a)
  Par l r       -> Par (l # x :/ a) (r # x :/ a)
  Let p q t b   -> Let p q (t # x :/ a) (b # x :/ a)
  Var v
    | v == x    -> a
    | otherwise -> f
infixl 1 #

type TermST = State (Int, Term) Term

termST :: TermST
termST = do
  (n, t) <- get
  let (x0,x1) = (n+1,n+2)
  case t of
    App (Abs x f) a     -> do put (n, f # x :/ a); termST
    Let p q (Par u v) t -> do put (n, t # (p :/ u) # (q :/ v)); termST
    App (Par u v) a     -> do
      put (n+2, Let x0 x1 a $ Par (App u $ Var x0) (App v $ Var x1))
      termST
    Let p q (Abs x f) t -> do
      let l = (Let p q f t) #
                (p :/ (Abs x0 (Var p))) #
                (q :/ (Abs x1 (Var q))) #
                (x :/ (Par (Var x0) (Var x1)))
      put (n+2, l)
      termST
    Par u v -> return $ Par (go n u) (go n v)
    App u v -> return $ App (go n u) (go n v)
    Abs h b -> return $ Abs h (go n b)
    Let p q t b -> return $ Let p q (go n t) (go n b)
    x -> return x
  where
    go m r = evalState termST (m, r)


reduce1 :: Term -> Term
reduce1 term = case term of
  App (Abs x f) a     -> f # x :/ a
  Let p q (Par u v) t -> t # (p :/ u) # (q :/ v)
  App (Par u v) a     -> Let x0 x1 a $ Par (App u $ Var x0) (App v $ Var x1)
  Let p q (Abs x f) t ->
    (Let p q f t) #
      p :/ (Abs x0 (Var p)) #
      q :/ (Abs x1 (Var q)) #
      x :/ (Par (Var x0) (Var x1))
  x -> x
  where
    (x0, x1) = (maxV term + 1, maxV term + 2)
reduce :: Term -> Term
reduce t = evalState termST (maxV t, t)

maxV :: Term -> Symb
maxV term = go 0 term
  where
    go n t = case t of
      Abs a b     -> max a (go n b)
      App a b     -> max (go n a) (go n b)
      Par a b     -> max (go n a) (go n b)
      Let p q a b -> max (go n a) (go n b)
      Var v       -> max v n

