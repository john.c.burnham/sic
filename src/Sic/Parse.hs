{-# LANGUAGE OverloadedStrings #-}
module Sic.Parse where

import           Data.Char
import           Data.Text                  (Text)
import qualified Data.Text                  as T
import           Data.Void                  (Void)
import           Text.Megaparsec
import           Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer as L

import           Sic.Type

type Parser = Parsec Void Text

spaceC = L.space space1 empty empty
lexeme = L.lexeme spaceC
symbol = L.symbol spaceC
parens = between (symbol "(") (symbol ")")
comma  = symbol ","
dot    = symbol "."

symb = L.decimal <|> (parseVar <$> some (oneOf ['a'..'z']))

parseVar :: String -> Int
parseVar s = go 0 0 ((\x -> ord x - 97 + 1) <$> s)
  where
    go e n (x:xs) = go (e + 1) (n + x * 26^e) xs
    go _ n []     = n - 1

term :: Parser Term
term = ti <|> parens ti
  where
    ti = lexeme $ choice
        [ try var_
        , try app_
        , try abs_
        , try par_
        , try let_
        ]

par_ :: Parser Term
par_ = parens $ do
  a <- term
  comma
  b <- term
  return $ Par a b

var_ :: Parser Term
var_ = Var <$> symb

abs_ = do
  symbol "\\"
  a <- symb
  dot
  b <- term
  return $ Abs a b

app_ = parens $ do
  a <- term
  b <- term
  return $ App a b

let_ = do
  symbol ":let"
  symbol "("
  p <- symb
  comma
  q <- symb
  symbol ")"
  symbol "="
  a <- term
  symbol ":in"
  b <- term
  return $ Let p q a b


