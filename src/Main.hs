module Main where

import qualified Data.Text.IO    as TIO
import           Sic.Eval
import           Sic.Parse
import           Sic.Type
import           Text.Megaparsec (parse)

runFile :: String -> IO ()
runFile f = do
  x <- TIO.readFile f
  print $ reduce <$> parse term f x

main :: IO ()
main = do
  putStrLn "hello world"
